//Ejercicio 1 
//Haz un bucle y muestra por consola todos aquellos valores del array que incluyan la palabra "Camiseta". Usa la función .includes de javascript.

/*
const products = ["Camiseta de Pokemon", "Pantalón coquinero", "Gorra de gansta", "Camiseta de Basket", "Cinrurón de Orión", "AC/DC Camiseta",];
    function showProducts() {
    for (let i = 0; i < products.length; i++) {
        if (products[i].includes("Camiseta")) {
        console.log(products[i]);
        }
    }
}
showProducts(products);
*/

//Ejercicio 2
// Comprueba en cada uno de los usuarios que tenga al menos dos trimestres aprobados y añade la propiedad isApproved a true o false en consecuencia. Una vez lo tengas compruébalo con un console.log.

/*
const alumns = [
    { name: "Pepe Viruela", T1: false, T2: false, T3: true },
    { name: "Lucia Aranda", T1: true, T2: false, T3: true },
    { name: "Juan Miranda", T1: false, T2: true, T3: true },
    { name: "Alfredo Blanco", T1: false, T2: false, T3: false },
    { name: "Mario Polo", T1: true, T2: true, T3: true },
];  
    for (let index = 0; index < alumns.length; index++) {
    alumn = alumns[index];

    let approvedCounter = 0;
    approvedCounter = alumn.T1 ? approvedCounter + 1 : approvedCounter;
    approvedCounter = alumn.T2 ? approvedCounter + 1 : approvedCounter;
    approvedCounter = alumn.T3 ? approvedCounter + 1 : approvedCounter;
    alumn.isApproved = approvedCounter >= 2 ? true : false;
}
console.log(alumns);
*/

//Ejercicio 3
// Usa un bucle forof para recorrer todos los destinos del array. Imprime en un console.log sus valores.
/*
const placesToTravel = ["Japon", "Venecia", "Murcia", "Santander", "Filipinas", "Madagascar",];
for (const places of placesToTravel) {
    console.log(places);
}
*/

//Ejercicio 4
// Usa un bucle forof para recorrer todos los destinos del array. Imprime en un console.log sus valores.
/*
const alien = {
    name: "Wormuck",
    race: "KuKusumusu",
    planet: "Eden",
    weight: "259kg",
};
    for (const key in alien) {
    console.log("Valor: " + key + ", Caracteristicas: " + alien[key]);
}
*/

//Ejercicio 5 
//Usa un bucle for para recorrer todos los destinos del array y elimina los elementos que tengan el id 11 y 40. Imprime en un console log el array. 
/*
const placesToTravel = [{ id: 5, name: "Japan" },{ id: 11, name: "Venecia" },{ id: 23, name: "Murcia" },{ id: 40, name: "Santander" },{ id: 44, name: "Filipinas" },{ id: 59, name: "Madagascar" },];
    for (let index = 0; index < placesToTravel.length; index++) {
    const element = placesToTravel[index];
        if (element.id === 11 || element.id === 40) {
        placesToTravel.splice(index, 1);
        }
}
console.log(placesToTravel);
*/

//Ejercicio 6
// Usa un bucle for para recorrer todos los juguetes y elimina los que incluyan la palabra gato. Recuerda que puedes usar la función .includes() para comprobarlo.
/*
const toys = [
    { id: 5, name: "Buzz MyYear" },
    { id: 11, name: "Action Woman" },
    { id: 23, name: "Barbie Man" },
    { id: 40, name: "El gato con Guantes" },
    { id: 40, name: "El gato felix" },
    { id: 23, name: "Barbie Man" }
];
for (let i = 0; i < toys.length; i++) {
        if (toys[i].name.includes("gato")) {
        toys.splice(i, 1);
        i--;
        }
    }
console.log(toys);
*/

//Ejercicio 7
// Usa un bucle for...of para recorrer todos los juguetes y añade los que tengan más de 15 ventas (sellCount) al array popularToys. Imprimelo por consola.
/*
const popularToys = [];
const toys = [
    { id: 5, name: "Buzz MyYear", sellCount: 10 },
    { id: 11, name: "Action Woman", sellCount: 24 },
    { id: 23, name: "Barbie Man", sellCount: 15 },
    { id: 40, name: "El gato con Guantes", sellCount: 8 },
    { id: 40, name: "El gato felix", sellCount: 35 },
];
for (const toy of toys) {
    if (toy.sellCount > 15) {
    popularToys.push(toy);
    }
}
console.log(popularToys);
*/
